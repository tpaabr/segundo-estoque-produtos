import React from 'react';

class CadastrarProduto extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            nomeProduto: "",
            valorProduto: 0,
            quantidadeProduto: 0
        }
    }

    handleChange = event => {
        if (event.target.name === "nomeProduto"){
            this.setState({
                nomeProduto: event.target.value,
                valorProduto: this.state.valorProduto
            })
        }else if(event.target.name === "valorProduto"){
            this.setState({
                nomeProduto: this.state.nomeProduto,
                valorProduto: event.target.value
            })
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let produtoAtual = this.state;
        this.props.addProduto(produtoAtual);

    }

    render(){
        return(
            <React.Fragment>
                <form>
                    <label>Nome do Produto</label>
                    <input 
                        type="text"
                        name="nomeProduto"
                        onChange={this.handleChange}
                    />
                    
                    <input
                        type="number"
                        name="valorProduto"
                        onChange={this.handleChange}
                    />

                    <button onClick={this.handleSubmit}>Cadastar Produto</button>
                </form>
            </React.Fragment>
        );
    }
}

export default CadastrarProduto;