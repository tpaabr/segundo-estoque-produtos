import React from 'react';

class EntradaSaida extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            lista: this.props.listaProdutos,
            nomeProdutoAtual: "",
            novaQuantidade: 0
        }
    }

    handleQuantidade = (event) => {
        console.log(event.target.value)
        let temp = event.target.value
        this.setState({
            lista: this.state.lista, 
            nomeProdutoAtual: this.nomeProdutoAtual, 
            novaQuantidade: temp
        });
        console.log(this.state)
    
    }
    render(){
        return(
            <React.Fragment>
                <form>
                    <select name="nome" onChange={this.handleChange}>
                        <option></option>
                        {this.state.lista.map((produto, index) => <option key={index} id={index} value={produto.nomeProduto}>{produto.nomeProduto}</option>)}
                    </select>
                    <input
                        type="number"
                        name="quantidade" 
                        onChange={this.handleQuantidade}
                    />
                    <button name="entrada">Entrar com essa quantidade</button>
                    <button name="saida">Sair com essa quantidade</button>
                </form>

            </React.Fragment>
        );
    }
}

export default EntradaSaida;