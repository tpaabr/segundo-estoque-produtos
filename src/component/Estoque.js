import React from 'react';
import CadastrarProduto from './CadastrarProduto';
import EntradaSaida from './EntradaSaida';

class Estoque extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            lista: []
        }

        this.lista = []
    }

    addProduto = (produto) => {

        let existe = false

        //Não deixar criar produto com mesmo nome
        this.state.lista.forEach(element => {
            if(element.nomeProduto === produto.nomeProduto){
                alert("Já existe um produto com esse nome")
                existe = true
            }
        });

        if(!existe){
            let novaLista = this.state.lista
            novaLista[novaLista.length] = produto
            
            this.setState({lista: novaLista})
        }
    }

    render(){
        return(
            <React.Fragment>
                <CadastrarProduto addProduto={this.addProduto}/>
                <EntradaSaida listaProdutos={this.state.lista}/>
                {this.state.lista.map((produto, index) => <p key={index} >Nome Produto:{produto.nomeProduto} || Preço: {produto.valorProduto} || Quantidade: {produto.quantidadeProduto}</p>)}
            </React.Fragment>
        );
    }
}

export default Estoque;
